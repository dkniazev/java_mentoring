package com.epam;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class FacadeFigure {
	private final Figure dot = new Dot();
	private final Figure circle = new Circle();

	public List<String> drawDots(int n, int x, int y) {
		List<String> result = new ArrayList<>(n);
		for (int i = 0; i < n; i++) {
			result.add(dot.draw(x, y));
		}
		return result;
	}

	public String drawDottedCircle(int x, int y) {
		return dot.draw(x, y) + circle.draw(x, y);
	}
}
