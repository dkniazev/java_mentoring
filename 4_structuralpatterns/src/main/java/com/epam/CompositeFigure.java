package com.epam;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class CompositeFigure implements Figure {
	private final List<Figure> childElements;

	public CompositeFigure() {
		childElements = new LinkedList<>();
	}

	public void add(Figure figure) {
		childElements.add(figure);
	}

	@Override
	public String draw(int centerX, int centerY) {
		return childElements.stream()
				.map(obj -> obj.draw(centerX, centerY))
				.collect(Collectors.joining(","));
	}
}
