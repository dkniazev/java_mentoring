package com.epam;

import java.util.concurrent.CountDownLatch;

/**
 * @author Dmitrii_Kniazev
 * @since 02/16/2017
 */
public class LiveLockApp {
	static final CountDownLatch LATCH = new CountDownLatch(2);

	public static void main(String[] args) {
		FileWithLock filePath1 = new FileWithLock("files\\test1.txt");
		FileWithLock filePath2 = new FileWithLock("files\\test2.txt");

		new Thread(new FileCopier(filePath1, filePath2)).start();
		new Thread(new FileCopier(filePath2, filePath1)).start();
	}
}
