package com.epam;

/**
 * @author Dmitrii_Kniazev
 * @since 03/03/2017
 */
public final class Person {
	private final String name;
	private final String address;

	public Person(String name, String address) {
		this.name = name;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}
}
