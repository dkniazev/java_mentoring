package com.epam;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class Circle implements Figure {
	@Override
	public String draw(int centerX, int centerY) {
		return String.format("circle:%d,%d", centerX, centerY);
	}
}
