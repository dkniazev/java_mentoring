package com.epam;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class FigureWithColor extends StyledFigure {
	private final String color;

	public FigureWithColor(Figure figure, String color) {
		super(figure);
		this.color = color + "_";
	}


	@Override
	protected String decorate(String src) {
		return color + src;
	}
}
