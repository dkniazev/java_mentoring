package com.epam

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Session
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.extras.codecs.jdk8.LocalDateCodec
import com.datastax.driver.mapping.Mapper
import com.datastax.driver.mapping.MappingManager
import com.epam.domains.Note
import org.slf4j.LoggerFactory
import java.util.ArrayList

/**
 * @author Dmitrii_Kniazev
 * @since 06/21/2017
 */

class App {
    companion object {
        private const val CASSANDRA_HOST = "192.168.99.100"
        private const val CASSANDRA_PORT = 9042
        private const val KEYSPACE = "mhw"
        private const val TABLE_NAME = "note_T"

        private val LOG = LoggerFactory.getLogger(App::class.java)

        @JvmStatic fun main(args: Array<String>) {
            val cluster: Cluster = Cluster.builder()
                    .addContactPoint(CASSANDRA_HOST)
                    .withPort(CASSANDRA_PORT)
                    .build()
            LOG.info("Cluster connected::$CASSANDRA_HOST:$CASSANDRA_PORT")
            //register JAVA8 "LocalDate" codec
            cluster.configuration.codecRegistry.register(LocalDateCodec.instance)
            val session: Session = cluster.connect()
            LOG.info("Session connected")
            prepareDatabase(session)

            val mapper = MappingManager(session).mapper(Note::class.java)
            LOG.info("Mapper created::${Note::class.qualifiedName}")
            val list = ArrayList<Note>()
            loop@ while (true) {
                val str = readLine()
                if (!str.isNullOrBlank()) {
                    val trimmedStr = str!!.trim()
                    when (trimmedStr.split(" ")[0]) {
                        "exit" -> break@loop
                        "list" -> printList(mapper)
                        "add" -> addEntity(mapper, trimmedStr.split(" ", limit = 2)[1])
                        "tag" -> {
                            list.clear()
                            list.addAll(findByTag(mapper, trimmedStr.split(" ", limit = 2)[1]))
                        }
                        "text" -> {
                            list.clear()
                            list.addAll(findInText(mapper, trimmedStr.split(" ", limit = 2)[1]))
                        }
                        "delete" -> {
                            delete(mapper, list)
                            list.clear()
                        }
                    }
                }
            }

            if (!session.isClosed) {
                session.close()
                LOG.info("Session closed")
            }
            if (!cluster.isClosed) {
                cluster.close()
                LOG.info("Cluster closed")
            }
        }

        private fun delete(mapper: Mapper<Note>, list: List<Note>) {
            if (list.isEmpty()) {
                println("Nothing deleted")
            } else {
                for (note in list) {
                    mapper.delete(note)
                    println("Deleted:$note")
                }
            }
        }

        private fun addEntity(mapper: Mapper<Note>, s: String) {
            val splittedStr = s.split("::")
            val tag = splittedStr[0]
            val text = splittedStr[1]
            val newNote = Note(tag = tag, body = text)
            mapper.save(newNote)
            println("New note saved:$newNote")
        }

        private fun printList(mapper: Mapper<Note>) {
            val select = QueryBuilder.select().all().from(KEYSPACE, TABLE_NAME)
            val resultSet = mapper.manager.session.execute(select)
            val result = mapper.map(resultSet)
            val list = result.all()
            if (list.isEmpty()) {
                println("Table is empty")
            } else {
                list.forEach { println(it) }
            }
        }

        private fun findByTag(mapper: Mapper<Note>, tag: String): List<Note> {
            val select = QueryBuilder.select()
                    .from(KEYSPACE, TABLE_NAME)
                    .allowFiltering()
                    .where(QueryBuilder.eq("tag", tag))
            val resultSet = mapper.manager.session.execute(select)
            val result = mapper.map(resultSet)
            val list = result.all()
            if (list.isEmpty()) {
                println("Nothing found for: \'$tag\'")
            } else {
                list.forEach { println(it) }
            }
            return list
        }

        private fun findInText(mapper: Mapper<Note>, text: String): List<Note> {
            val select = QueryBuilder.select()
                    .from(KEYSPACE, TABLE_NAME)
                    .where(QueryBuilder.like("body", "%$text%"))
            val resultSet = mapper.manager.session.execute(select)
            val result = mapper.map(resultSet)
            val list = result.all()
            if (list.isEmpty()) {
                println("Nothing found for: '$text'")
            } else {
                list.forEach { println(it) }
            }
            return list
        }

        private fun prepareDatabase(session: Session) {
            val createKeyspaceCql = "CREATE KEYSPACE IF NOT EXISTS $KEYSPACE" +
                    " WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 2};"
            val createTableCql = "CREATE TABLE IF NOT EXISTS $KEYSPACE.$TABLE_NAME (" +
                    " uuid uuid PRIMARY KEY," +
                    " creation_date date," +
                    " tag text," +
                    " body text" +
                    " ) WITH comment='Notes table';"
            val createIndex = "CREATE CUSTOM INDEX if not exists body_idx ON $KEYSPACE.$TABLE_NAME (body)" +
                    " USING 'org.apache.cassandra.index.sasi.SASIIndex'" +
                    " WITH OPTIONS = {" +
                    " 'analyzed' : 'true'," +
                    " 'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.NonTokenizingAnalyzer'," +
                    " 'mode' : 'CONTAINS'," +
                    " 'case_sensitive': 'false'};"
            session.execute(createKeyspaceCql)
            session.execute(createTableCql)
            session.execute(createIndex)
            LOG.info("Database prepared")
        }
    }
}
