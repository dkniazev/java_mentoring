package com.epam;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Scanner;

/**
 * @author Dmitrii_Kniazev
 * @since 02/16/2017
 */
public class Main {

    private static final String CLASS = "com.epam.operation.Operation";

    public static void main(String[] args)
            throws MalformedURLException,
            ClassNotFoundException,
            NoSuchMethodException,
            InvocationTargetException,
            IllegalAccessException {
        final String libsPath = args.length < 1 ? "./libs" : args[0];
        final File[] jarFiles = new File(libsPath)
                .listFiles((dir, name) ->
                        !name.startsWith("calc") && name.endsWith(".jar"));
        int length = jarFiles.length;
        final URLClassLoader[] classLoaders = new URLClassLoader[length];
        for (int i = 0; i < length; i++) {
            final URL[] url = new URL[1];
            url[0] = jarFiles[i].toURI().toURL();
            classLoaders[i] = new URLClassLoader(url);
        }

        System.out.printf("Fined %d implementations\n", length);
        for (int i = 0; i < jarFiles.length; i++) {
            File jarFile = jarFiles[i];
            System.out.printf("%d: %s\n", i, jarFile.getName());
        }
        final Scanner in = new Scanner(System.in);
        while (true) {
            final String line = in.nextLine();
            if ("exit".equals(line)) break;
            try {
                int num = Integer.parseInt(line);
                if (num < 0 || num >= length) {
                    System.out.println("Wrong number: " + num);
                } else {
                    final URLClassLoader classLoader = classLoaders[num];
                    final Class<?> aClass = classLoader.loadClass(CLASS);
                    final Method method = aClass.getMethod(
                            "doOperation",
                            Integer.class,
                            Integer.class);
                    System.out.println("Enter first arg");
                    final Integer firstArg = in.nextInt();
                    System.out.println("Enter second arg");
                    final Integer secondArg = in.nextInt();
                    Integer result = (Integer) method.invoke(null, firstArg, secondArg);
                    System.out.println("Result: " + result);
                }
            } catch (NumberFormatException ex) {
                System.out.println("It is not number:" + line);
            }
        }
    }
}
