package com.epam.consumers;

import com.epam.Person;

/**
 * Q: Why singleton approach was chosen for the file?<br/>
 * A: Difficult concurrent access to file modification<br/>
 *
 * @author Dmitrii_Kniazev
 * @since 03/03/2017
 */
final class FileConsumer implements IConsumer {
	private FileConsumer() {
	}

	@Override
	public Person updateAddress(String name, String address) {
		System.out.println("File record updated");
		return new Person(name, address);
	}

	public static FileConsumer getInstance() {
		return Holder.INSTANCE;
	}

	private static class Holder {
		private static final FileConsumer INSTANCE = new FileConsumer();
	}
}
