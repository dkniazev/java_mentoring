#Creational Patterns
Write a Java Code for the next model:  
A data consumer extracts and can update data from a data source.  
Data consumer: customer’s card  
Data: person’s name and address  
Data source: a) database or b) one and only one File from a file system.

Use two patterns for your implementation:
1) Factory method
2) Singleton to retrieve data from the file. In comments for a singleton class try to explain,
 please, why singleton approach was chosen for the file. Just couple of words.

Create two implementations of Singleton:  
a) with a lazy initialization  
b) with not a lazy initialization.  
Use a solution of Joshua Bloch  
Additionally:
Structure of application is important; methods realization doesn't matter. 
You can leave them blank if it doesn’t affect the structure.