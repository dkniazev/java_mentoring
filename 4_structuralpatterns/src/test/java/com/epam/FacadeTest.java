package com.epam;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class FacadeTest {
	@Test
	public void testFacade() {
		final int x = 5;
		final int y = 46;

		final FacadeFigure facade = new FacadeFigure();
		final int n = 7;
		final List<String> actual = facade.drawDots(n, x, y);
		Assert.assertEquals(n, actual.size());
		final String dotString = new Dot().draw(x, y);
		final String circleString = new Circle().draw(x, y);
		for (String s : actual) {
			Assert.assertEquals(s, dotString);
		}

		Assert.assertEquals(dotString + circleString, facade.drawDottedCircle(x, y));
	}
}
