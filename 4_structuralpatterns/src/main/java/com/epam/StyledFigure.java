package com.epam;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public abstract class StyledFigure implements Figure {
	protected final Figure figure;

	public StyledFigure(Figure figure) {
		this.figure = figure;
	}

	protected abstract String decorate(String src);

	@Override
	public String draw(int centerX, int centerY) {
		return decorate(figure.draw(centerX, centerY));
	}
}
