package com.epam;

import com.epam.adapters.CircleAdapter;
import com.epam.adapters.DotAdapter;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class AdaptersTest {
	private final int leftX = 1;
	private final int topY = 3;
	private final int rightX = 3;
	private final int bottomY = 1;

	private final int centerX = 2, centerY = 2;

	@Test
	public void testDotAdapter() {
		final Figure dot = new Dot();
		final FrameworkFigure frameworkDot = new DotAdapter();
		final String expected = dot.draw(centerX, centerY);
		final String actual = frameworkDot.paint(leftX, topY, rightX, bottomY);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCircleAdapter() {
		final Figure circle = new Circle();
		final FrameworkFigure frameworkCircle = new CircleAdapter();
		final String expected = circle.draw(centerX, centerY);
		final String actual = frameworkCircle.paint(leftX, topY, rightX, bottomY);
		Assert.assertEquals(expected, actual);
	}
}
