package com.epam;

import java.io.File;
import java.net.URI;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Dmitrii_Kniazev
 * @since 02/16/2017
 */
public class FileWithLock extends File {
	private final Lock lock = new ReentrantLock();

	public FileWithLock(String pathname) {
		super(pathname);
	}

	public FileWithLock(String parent, String child) {
		super(parent, child);
	}

	public FileWithLock(File parent, String child) {
		super(parent, child);
	}

	public FileWithLock(URI uri) {
		super(uri);
	}

	public Lock getLock() {
		return this.lock;
	}
}
