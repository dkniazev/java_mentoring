package com.epam.consumers;

import com.epam.Person;

/**
 * @author Dmitrii_Kniazev
 * @since 03/03/2017
 */
public interface IConsumer {
	Person updateAddress(final String name, final String address);
}
