package com.epam;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public interface FrameworkFigure {
	String paint(int leftX, int topY, int rightX, int bottomY);
}
