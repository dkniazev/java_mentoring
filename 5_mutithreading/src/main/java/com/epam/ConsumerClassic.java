package com.epam;

import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Dmitrii Kniazev
 * @since 11.06.2017
 */
public class ConsumerClassic implements Runnable {
    private final Queue<Integer> queue;
    private static final AtomicLong COUNTER = new AtomicLong(0);
    private final CountDownLatch latch;

    public ConsumerClassic(final Queue<Integer> queue, CountDownLatch latch) {
        this.queue = queue;
        this.latch = latch;
    }

    @Override
    public void run() {
        final long startTime = System.nanoTime();
        System.out.println("Consumer Classic started:" + startTime);
        while (!Thread.interrupted()) {
            synchronized (queue) {
                if (!queue.isEmpty()) {
                    final Integer poll = queue.poll();
                    final long num = COUNTER.incrementAndGet();
                }
            }
        }
        long stoppedTime = System.nanoTime();
        System.out.println("Consumer Classic stopped:" + startTime);
        final long time = stoppedTime - startTime;
        System.out.println("Consumer Classic total:"
                + COUNTER.toString()
                + "/" + time / 1000_000_000 + "s");
        latch.countDown();
    }


}
