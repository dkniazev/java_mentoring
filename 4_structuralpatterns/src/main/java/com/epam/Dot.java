package com.epam;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class Dot implements Figure {
	@Override
	public String draw(int centerX, int centerY) {
		return String.format("dot:%d,%d", centerX, centerY);
	}
}
