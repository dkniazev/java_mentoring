package com.epam;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Dmitrii Kniazev
 * @since 11.06.2017
 */
public class ConsumerConcurrent implements Runnable {
    private final BlockingQueue<Integer> queue;
    private static final AtomicLong COUNTER = new AtomicLong(0);
    private final CountDownLatch latch;

    public ConsumerConcurrent(final BlockingQueue<Integer> queue, CountDownLatch latch) {
        this.queue = queue;
        this.latch = latch;
    }

    @Override
    public void run() {
        final long startTime = System.nanoTime();
        System.out.println("Consumer Concurrent started:" + startTime);
        while (!Thread.interrupted()) {
            try {
                final Integer poll = queue.take();
                final long num = COUNTER.incrementAndGet();
            } catch (InterruptedException e) {
                System.out.println("TT1");
                e.printStackTrace();
            }
        }
        long stoppedTime = System.nanoTime();
        System.out.println("Consumer Concurrent stopped:" + startTime);
        final long time = stoppedTime - startTime;
        System.out.println("Consumer Concurrent total:"
                + COUNTER.toString()
                + "/" + time / 1000_000_000 + "s");
        latch.countDown();
    }
}
