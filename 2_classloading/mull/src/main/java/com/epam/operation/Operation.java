package com.epam.operation;

/**
 * @author Dmitrii Kniazev
 * @since 24.02.2017
 */
public class Operation {
    public static int doOperation(Integer a, Integer b) {
        return a * b;
    }
}
