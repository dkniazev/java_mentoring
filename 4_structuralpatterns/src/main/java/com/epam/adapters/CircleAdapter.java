package com.epam.adapters;

import com.epam.Circle;
import com.epam.Figure;
import com.epam.FrameworkFigure;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class CircleAdapter implements FrameworkFigure {
	private final Figure figure = new Circle();

	@Override
	public String paint(int leftX, int topY, int rightX, int bottomY) {
		int centerX = leftX + (rightX - leftX) / 2;
		int centerY = bottomY + (topY - bottomY) / 2;
		return figure.draw(centerX, centerY);
	}
}
