package com.epam.consumers;

import com.epam.Person;

/**
 * @author Dmitrii_Kniazev
 * @since 03/03/2017
 */
final class DbConsumer implements IConsumer {
	private static final DbConsumer INSTANCE = new DbConsumer();

	private DbConsumer() {
	}

	@Override
	public Person updateAddress(String name, String address) {
		System.out.println("DB record updated");
		return new Person(name, address);
	}

	public static DbConsumer getInstance() {
		return DbConsumer.INSTANCE;
	}
}
