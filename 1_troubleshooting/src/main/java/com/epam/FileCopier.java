package com.epam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.locks.Lock;

/**
 * @author Dmitrii_Kniazev
 * @since 02/16/2017
 */
public class FileCopier implements Runnable {
	private final FileWithLock inFile;
	private final FileWithLock outFile;

	FileCopier(FileWithLock inPath, FileWithLock outPath) {
		this.inFile = inPath;
		this.outFile = outPath;
	}

	@Override
	public void run() {
		final Lock inLock = inFile.getLock();
		while (true) {
			if (inLock.tryLock()) {
				try (BufferedReader in = new BufferedReader(new FileReader(inFile))) {

					LiveLockApp.LATCH.countDown();
					LiveLockApp.LATCH.await();

					final Lock outLock = outFile.getLock();
					while (true) {
						if (outLock.tryLock()) {
							try (BufferedWriter out = new BufferedWriter(new FileWriter(outFile))) {
								String line = in.readLine();
								while (line != null) {
									out.write(line);
								}
								break;
							} finally {
								outLock.unlock();
							}
						}
					}
					break;
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				} finally {
					inLock.unlock();
				}
			}
		}
	}
}
