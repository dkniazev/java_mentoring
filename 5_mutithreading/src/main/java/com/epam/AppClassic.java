package com.epam;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Dmitrii_Kniazev
 * @since 11/06/2017
 */
public class AppClassic extends Thread {
    private static final int N_THREADS = 2;
    private static final ExecutorService EXECUTOR_SERVICE
            = Executors.newFixedThreadPool(N_THREADS);
    private static final CountDownLatch LATCH = new CountDownLatch(2);

    public AppClassic() {
        super();
    }

    public static void main(String[] args) {
        final Queue<Integer> queue = new LinkedList<>();
        final ProducerClassic producerClassic = new ProducerClassic(queue, LATCH);
        final ConsumerClassic consumerClassic = new ConsumerClassic(queue, LATCH);
        EXECUTOR_SERVICE.execute(consumerClassic);
        EXECUTOR_SERVICE.execute(producerClassic);
        Runtime.getRuntime().addShutdownHook(new AppClassic());
    }

    @Override
    public void run() {
        EXECUTOR_SERVICE.shutdownNow();
        try {
            LATCH.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Executor service shutdown");
    }
}
