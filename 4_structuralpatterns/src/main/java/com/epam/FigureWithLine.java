package com.epam;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class FigureWithLine extends StyledFigure {
	private final String line;

	public FigureWithLine(Figure figure, String line) {
		super(figure);
		this.line = line + "_";
	}


	@Override
	protected String decorate(String src) {
		return line + src;
	}
}
