package com.epam;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Dmitrii_Kniazev
 * @since 11/06/2017
 */
public class AppConcurrent extends Thread {
    private static final int N_THREADS = 2;
    private static final ExecutorService EXECUTOR_SERVICE
            = Executors.newFixedThreadPool(N_THREADS);
    private static final CountDownLatch LATCH = new CountDownLatch(2);

    public AppConcurrent() {
        super();
    }

    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue = new LinkedBlockingQueue<>();
        final ProducerConcurrent producerConcurrent
                = new ProducerConcurrent(blockingQueue, LATCH);
        final ConsumerConcurrent consumerConcurrent
                = new ConsumerConcurrent(blockingQueue, LATCH);
        EXECUTOR_SERVICE.execute(producerConcurrent);
        EXECUTOR_SERVICE.execute(consumerConcurrent);
        Runtime.getRuntime().addShutdownHook(new AppConcurrent());
    }

    @Override
    public void run() {
        EXECUTOR_SERVICE.shutdownNow();
        try {
            LATCH.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Executor service shutdown");
    }
}
