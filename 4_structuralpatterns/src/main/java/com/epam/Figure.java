package com.epam;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public interface Figure {
	String draw(int centerX, int centerY);
}
