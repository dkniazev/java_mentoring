package com.epam;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class CompositeTest {

	@Test
	public void testComposite() {
		final int x = 5;
		final int y = 46;

		final Dot dot = new Dot();
		final Circle circle = new Circle();
		final String dotString = dot.draw(x, y);
		final String circleString = circle.draw(x, y);

		final CompositeFigure simple = new CompositeFigure();
		simple.add(dot);
		simple.add(circle);
		final String simpleString = simple.draw(x, y);
		Assert.assertEquals(dotString + "," + circleString, simpleString);


		final CompositeFigure complex = new CompositeFigure();
		complex.add(circle);
		complex.add(simple);

		Assert.assertEquals(circleString + "," + simpleString, complex.draw(x, y));
	}
}
