package com.epam;


import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Dmitrii_Kniazev
 * @since 19/06/2017
 */
public class AppMetaspace {
    public static void main(String[] args) throws InterruptedException, MalformedURLException, ClassNotFoundException {
        final URL[] resource = {AppMetaspace.class.getClassLoader().getResource("libs/spring-core-1.2.6.jar")};
        final List<URLClassLoader> loaderList = new LinkedList<>();
        for (; ; ) {
            final URLClassLoader classLoader = new URLClassLoader(resource);
            final Class<?> aClass = classLoader.loadClass("org.springframework.util.Assert");
            loaderList.add(classLoader);
        }
    }
}
