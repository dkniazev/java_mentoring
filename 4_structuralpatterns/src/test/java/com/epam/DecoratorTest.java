package com.epam;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class DecoratorTest {
	@Test
	public void testDecorator() {
		final int x = 5;
		final int y = 46;

		final Dot dot = new Dot();
		final Circle circle = new Circle();
		final String red = "red";
		final String green = "green";

		final FigureWithColor redDot = new FigureWithColor(dot, red);
		final String redDotExpected = red + "_" + dot.draw(x, y);
		Assert.assertEquals(redDotExpected, redDot.draw(x, y));

		final String thickLine = "thick";
		final String thickCircleExpected = thickLine + "_" + circle.draw(x, y);
		final FigureWithLine thickCircle = new FigureWithLine(circle, thickLine);
		Assert.assertEquals(thickCircleExpected, thickCircle.draw(x, y));

		final FigureWithColor greenThickCircle = new FigureWithColor(thickCircle, green);
		final String greenThickCircleExpected = green + "_" + thickLine + "_" + circle.draw(x, y);
		Assert.assertEquals(greenThickCircleExpected, greenThickCircle.draw(x, y));

	}
}
