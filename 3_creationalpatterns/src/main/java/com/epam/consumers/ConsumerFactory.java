package com.epam.consumers;

/**
 * @author Dmitrii_Kniazev
 * @since 03/03/2017
 */
public final class ConsumerFactory {
	public static IConsumer createDbConsumer() {
		return DbConsumer.getInstance();
	}

	public static IConsumer createFileConsumer() {
		return FileConsumer.getInstance();
	}
}
