package com.epam;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Dmitrii Kniazev
 * @since 11.06.2017
 */
public class ProducerConcurrent implements Runnable {
    private final BlockingQueue<Integer> queue;
    private final Random random;
    private static final AtomicLong COUNTER = new AtomicLong(0);
    private final CountDownLatch latch;

    public ProducerConcurrent(final BlockingQueue<Integer> queue, CountDownLatch latch) {
        this.queue = queue;
        random = new Random();
        this.latch = latch;
    }

    @Override
    public void run() {
        final long startTime = System.nanoTime();
        System.out.println("Producer Classic started:" + startTime);
        while (!Thread.interrupted()) {
            final int nextInt = random.nextInt();
            try {
                queue.put(nextInt);
                final long num = COUNTER.incrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long stoppedTime = System.nanoTime();
        System.out.println("Producer Classic stopped:" + startTime);
        final long time = stoppedTime - startTime;
        System.out.println("Producer Classic total:"
                + COUNTER.toString()
                + "/" + time / 1000_000_000 + "s");
        latch.countDown();
    }
}
