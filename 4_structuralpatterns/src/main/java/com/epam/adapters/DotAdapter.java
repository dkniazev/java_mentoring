package com.epam.adapters;

import com.epam.Dot;
import com.epam.Figure;
import com.epam.FrameworkFigure;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class DotAdapter implements FrameworkFigure {
	private final Figure figure = new Dot();

	@Override
	public String paint(int leftX, int topY, int rightX, int bottomY) {
		int centerX = leftX + (rightX - leftX) / 2;
		int centerY = bottomY + (topY - bottomY) / 2;
		return figure.draw(centerX, centerY);
	}
}
