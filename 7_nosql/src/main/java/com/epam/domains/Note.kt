package com.epam.domains

import com.datastax.driver.mapping.annotations.Column
import com.datastax.driver.mapping.annotations.PartitionKey
import com.datastax.driver.mapping.annotations.Table
import java.time.LocalDate
import java.util.UUID


/**
 * @author Dmitrii_Kniazev
 * @since 06/22/2017
 */
@Table(keyspace = "mhw", name = "note_t")
data class Note(
        @PartitionKey
        @Column(name = "uuid") var uuid: UUID = UUID.randomUUID(),
        @Column(name = "creation_date") var creationDate: LocalDate = LocalDate.now(),
        @Column(name = "tag") var tag: String? = null,
        @Column(name = "body") var body: String? = null){
    override fun toString(): String {
        return "date=$creationDate;\ttag=$tag;\ttext=$body"
    }
}
