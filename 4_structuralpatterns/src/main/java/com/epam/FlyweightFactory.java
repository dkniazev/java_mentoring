package com.epam;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class FlyweightFactory {
	private static final String DOT = "dot";
	private static final String CIRCLE = "circle";
	private final Map<String, Figure> map;

	public FlyweightFactory() {
		map = new HashMap<>(5);
	}

	public Figure getFigure(String name) {
		switch (name) {
			case DOT:
				return lookup(DOT, Dot.class);
			case CIRCLE:
				return lookup(CIRCLE, Circle.class);
			default:
				throw new IllegalArgumentException("Wrong figure");
		}
	}

	private Figure lookup(String name, Class<? extends Figure> clazz) {
		if (!map.containsKey(name)) {
			try {
				map.put(name, clazz.newInstance());
			} catch (InstantiationException | IllegalAccessException e) {
				throw new IllegalArgumentException(e);
			}
		}
		return map.get(name);
	}
}
