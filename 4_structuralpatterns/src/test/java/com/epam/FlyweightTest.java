package com.epam;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitrii_Kniazev
 * @since 03/10/2017
 */
public class FlyweightTest {
	@Test
	public void testFlyweight() {
		final FlyweightFactory factory = new FlyweightFactory();
		final String dotName = "dot";
		final Figure dot = factory.getFigure(dotName);
		Assert.assertTrue(dot == factory.getFigure(dotName));
		Assert.assertFalse(new Dot() == factory.getFigure(dotName));

		final String circleName = "circle";
		final Figure circle = factory.getFigure(circleName);
		Assert.assertTrue(circle == factory.getFigure(circleName));
		Assert.assertFalse(new Circle() == factory.getFigure(dotName));
	}
}
